extends Control



func _on_back_pressed():
	get_tree().change_scene_to_file("res://scenes/Menu/menu_main.tscn")

func _on_meta_clicked(url):
	if OS.get_name() == "HTML5":
		JavaScriptBridge.eval("window.open(" + url + ", '_blank')")
	else:
		OS.shell_open(url)
