extends Node


# Called when the node enters the scene tree for the first time.
func _ready():
	## We send the signal that the game has started
	GlobalEventHandler.start_game()
	GlobalInputHandler.in_game = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
