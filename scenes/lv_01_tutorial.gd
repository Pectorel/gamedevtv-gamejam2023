extends Node

@export var colors = [
	Color("de4141"),
	Color("b555b9")
]

# Called when the node enters the scene tree for the first time.
func _ready():
	DimensionSettings.set_colors(colors)
