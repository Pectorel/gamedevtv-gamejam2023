class_name AudioPlayer
extends Node

@export var music : AudioStream
var can_music_start := true


func start_audio():
	if(can_music_start):
		AudioHandler.add_audio(music)
		can_music_start = false


