class_name DimensionColor
extends Node

var parent : Node
@export var modulate_self := false
@export var parent_is_owner := true
@export var always_active := false

# Called when the node is ready
func _ready() -> void:
	if(parent_is_owner):
		parent = get_owner()
	else :
		parent = get_parent()
	GlobalEventHandler.dimension_switched.connect(_on_dimension_switched)
	GlobalEventHandler.game_started.connect(_on_game_started)


func switch_color():
	if(modulate_self):
		parent.self_modulate = DimensionSettings.get_color(parent.dimension, always_active)
	else:
		parent.modulate = DimensionSettings.get_color(parent.dimension, always_active)

# =============== Signals functions =================
## Called when dimension is switched
func _on_dimension_switched(_dimension) -> void:
	switch_color()

func _on_game_started() -> void:
	switch_color()
