class_name ComboZone
extends Node2D

@export var active := true
@export var areas: Array[NodePath]
var areas_nodes: Array[Area2D]

func _ready():
	
	for area in areas:
		areas_nodes.push_back(get_node(area))
	
	
	
#	for area in areas:
#		print(area)
#		area.body_entered.connect(area._on_area_enter)
#		area.body_exited.connect(area._on_area_exit)
#
		
	

func set_active(enabled : bool):
	active = enabled
	for area in areas_nodes:
		area.set_collision_mask_value(1, active)

func _process(_delta):
		for area in areas_nodes:
			area.visible = active

