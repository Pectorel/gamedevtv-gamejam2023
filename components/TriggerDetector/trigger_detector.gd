class_name TriggerDetector
extends Node

@export var trigger : Area2D
var parent : Node2D

signal player_entered
signal player_exited

func _ready():
	parent = get_owner()
	trigger.body_entered.connect(_on_body_entered)
	trigger.body_exited.connect(_on_body_exited)



func _on_body_entered(body):
	if(body.name == "player"):
		player_entered.emit(body)


func _on_body_exited(body):
	if(body.name == "player"):
		player_exited.emit(body)
