extends Node2D

@onready var trigger_detector : TriggerDetector = $Behavior/TriggerDetector
@onready var audio_player : AudioPlayer = $Behavior/AudioPlayer

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	trigger_detector.player_entered.connect(_on_player_entered)

func _on_player_entered(player: Player) -> void:
	audio_player.start_audio()
