extends Node2D


@export var trigger_detector : TriggerDetector

# Called when the node enters the scene tree for the first time.
func _ready():
	if(trigger_detector != null):
		trigger_detector.player_entered.connect(_on_player_entered)


func _on_player_entered(player: Player):
	GlobalEventHandler.win_game()

