extends DimensionProp

@onready var collision_detector := $Behavior/TriggerDetector
@onready var combo_zone : ComboZone = $ComboZone
@export var combo_zone_enabled := true

## Initialize event handler and child nodes
func _ready():
	collision_detector.player_entered.connect(game_over)
	combo_zone.set_active(combo_zone_enabled)
	super()


## Call game over event
func game_over(_player : Player):
	GlobalEventHandler.loose_game()

