extends Node2D

@export_range(0,1) var dimension := 1

# Called when the node enters the scene tree for the first time.
func _ready():
	var i = 0
	for spike in get_children():
		spike.dimension = dimension
		if(i != 0):
			spike.get_node("ComboZone").set_active(false)
		i += 1
