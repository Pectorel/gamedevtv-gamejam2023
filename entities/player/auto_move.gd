class_name AutoMove
extends Node

var player : CharacterBody2D
# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

@export var speed = 250

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_owner()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	
	# Add the gravity.
	if not player.is_on_floor():
		player.velocity.y += gravity * delta
	
	player.velocity.x = speed
	
	player.move_and_slide()
	
