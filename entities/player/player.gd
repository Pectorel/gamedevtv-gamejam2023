class_name Player
extends CharacterBody2D

##
##	Player specific script
##
## Contains the player specific behaviors and connects them
##

@export var auto_move : AutoMove

## Called when the node enters the scene tree for the first time.
func _ready():
	GlobalEventHandler.game_over.connect(_on_game_over)
	GlobalEventHandler.game_started.connect(_on_game_start)
	GlobalEventHandler.game_won.connect(_on_game_won)


## Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass


## Player die animation (not done for now) and stop moving
func die():
	if(auto_move != null):
		auto_move.queue_free()
		$Sprite2D.queue_free()


# ================= Signals functions ===================
## Called when game starts
func _on_game_start():
	pass


## Called when Game Over
func _on_game_over():
	die()

func _on_game_won():
	auto_move.queue_free()

