class_name DimensionHandler
extends Node

var player : PhysicsBody2D

func _ready():
	player = get_owner()
	GlobalEventHandler.dimension_switched.connect(_on_dimension_switched)
	GlobalEventHandler.game_started.connect(_on_game_started)


## Switch dimension logic
func switch_dimension(dimension):
	# Since collision layers are 2 and 3, and dimensions are 0 and 1,
	# we add 2 to the current dimension to match the collision layers
	set_colision_layer(dimension + 2)
	set_mask_layer(dimension + 2)
	set_color()


## Set correct collision layers based on dimension
func set_colision_layer(layer):
	# Reset all collision layers
	for i in range(2,4):
		player.set_collision_layer_value(i, false)
	# We set the current dimension layer to true to detect only the objects that are 
	# in the correct dimension
	player.set_collision_layer_value(layer, true)

func set_mask_layer(layer):
	# Reset all collision layers
	for i in range(2,4):
		player.set_collision_mask_value(i, false)
	# We set the current dimension layer to true to detect only the objects that are 
	# in the correct dimension
	player.set_collision_mask_value(layer, true)

## Set player color to current dimension
func set_color():
	player.modulate = DimensionSettings.get_color(DimensionSettings.dimension)


# ================ Signals Functions ===================
## Called when dimension is switched
func _on_dimension_switched(dimension):
	switch_dimension(dimension)


## Called when game has just started
func _on_game_started():
	switch_dimension(DimensionSettings.dimension)
