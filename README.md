# Repo for the GameDevTV Game Jam 2023

This is a public repo containing the Godot project of the game I've made for the GameDevTV Game Jam 2023.

## Theme : Life in 2 dimensions

## Duality

Branch for the ethereal world game project. In this game the player will be able to switch between 2 dimensions, indicated by the color of the square.
The player will have to switch between dimensions to avoid obstacles, this game is inspired by Geometry Dash.

The level 1 prototype is in the `scenes` folder. Load this level to have a preview of the game

## Links

- [Itch.io](https://pectorel.itch.io/)
- [Twitter](https://twitter.com/Pectorel)

## Licenses

### Sounds

- Electric_Bells_Loop_by_DSQT_125_bpm by Disquantic [Link](https://freesound.org/people/Disquantic/sounds/474794/) under the [CC4 4.0 License](https://creativecommons.org/licenses/by/4.0/)

### Graphics 

- Kenney "Input Prompts Pixel 16×" Assets [Link](https://kenney.nl/assets/input-prompts-pixel-16)
