class_name RythmAudioManager
extends Node

##
##	Basic audio manager 
##
## Audio manager that allows to add and remove global audio to scene easily
##
##

## Prefix to node_names for easier debuggin in scene
var node_prefix = "Audio_"
## Last id that has been created
var last_id : int = 1


## When Node is ready
func _ready():
	GlobalEventHandler.game_over.connect(_on_game_over)


## Creates an AudioStreamPlayer with given AudioStream resource. [br]Returns 
## the id of the created AudioStreamPlayer
func add_audio(audio_stream : AudioStream) -> int:
	# We create a new AudioStreamPlayer
	var audio_player := AudioStreamPlayer.new()
	# We set the audio to the given audio_stream parameter
	audio_player.set_stream(audio_stream)
	# We change the node name to match our prefix + id
	audio_player.name = node_prefix + str(last_id)
	# We auto_increment the ID
	last_id += 1
	# We add the audioStreamPlayer to the AudioNode
	add_child(audio_player)
	# We start playing the audio
	audio_player.play()
	return last_id


## Stop playing a specific audio
func stop_audio(id) -> void:
	var audio_node = get_audio_node(id)
	if(audio_node != null):
		audio_node.stop()


## Start to play a specific audio
func play_audio(id) -> void:
	var audio_node = get_audio_node(id)
	if(audio_node != null):
		audio_node.play()


## Completely removes an audio node
func remove_audio(id, _transition = "fade-out") -> void:
	var audio_node = get_audio_node(id)
	if(audio_node != null):
		audio_node.queue_free()
	


## Get the wanted audio with parameter id. [br]Returns
## null or AudioStreamPlayer if node is not found
func get_audio_node(id):
	var res = null
	if(get_node("/root/AudioHandler").has_node(node_prefix + str(id))):
		res = get_node("/root/AudioHandler/" + node_prefix + str(id))
	return res


## Remove all AudioStreamPlayer Nodes
func remove_all() -> void:
	for audio_player in get_children():
		audio_player.queue_free()


# ================== Signal function ==================
## Called when game over
func _on_game_over():
	remove_all()
