class_name GlobalSignalHandler
extends Node

##
## Send global signal catchable by any entities
##
## Serves as a signal container for all signals that can be catchable by any element of the game.
## Do not forget to add this script in autoload for it to work!!!
##

## Indicate that the dimension has been switched
signal dimension_switched

## Indicates that the game has started
signal game_started

## Indicates a GameOver
signal game_over

## Indicates a pause
signal pause

## Indicates a retry of the level
signal retry

## Indicates that the player has won
signal game_won

## Checks for input needed to send global signal
func _input(event) -> void:
	# On switch dimension we call the set_dimension method
	if (event.is_action_pressed("switch_dimension")):
		var dimension = 1 if event.button_index == 2 else 0
		set_dimension(dimension)

## Set current dimension and indicates that the dimension has changed. [br]Signal
## is sent with the new dimension integer
func set_dimension(dimension: int) -> void:
	dimension_switched.emit(dimension)

## Start game
func start_game() -> void:
	game_started.emit()

## Loose Game
func loose_game() -> void:
	game_over.emit()

func win_game() -> void:
	game_won.emit()
