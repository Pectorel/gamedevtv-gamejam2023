class_name DimensionBehavior
extends Node

##
## Contains all the Dimension global logic
##
## Autoload that contains all the global logic behind the dimensions
##

## Current dimension that the player is in
var dimension = 0
## Basic dimensions colors
var colors := [
	Color("de4141"),
	Color("b555b9")
]
## Array that will contain the alpha colors
var alpha_colors := []
## Alpha coefficient (0 to 1)
var alpha := 0.4


## When node enters the scene
func _ready() -> void:
	set_alpha_colors()
	GlobalEventHandler.dimension_switched.connect(_on_dimension_switch)


## Set a specific color
func set_color(index: int, color: Color) -> void:
	colors[index] = color
	set_alpha_colors()


## Set all colors
func set_colors(new_colors: Array) -> void:
	colors = new_colors
	set_alpha_colors()


## Set alpha_colors based on colors
func set_alpha_colors() -> void:
	alpha_colors = colors.duplicate()
	var i = 0
	for color in colors :
		alpha_colors[i] = Color(color.to_html(), alpha)
		i += 1


## Set dimension to specified dim parameter
func set_dimension(dim: int):
	# Specify dimension range (here only 2 dimension, 0 or 1)
	if(dim >= 0 && dim <= 1):
		dimension = dim


## Returns color based on given object_dimension
func get_color(object_dim : int = 1, force_active = false) -> Color:
	# If the object dim is in the current dimension, then send full color
	# Else send alpha color for non active element (element will have less opacity)
	return colors[object_dim] if object_dim == dimension || force_active else alpha_colors[object_dim]


# =========== Signals function ===================
## Called when dimension is switched
func _on_dimension_switch(dim):
	set_dimension(dim)
