class_name GameWonScript
extends CanvasLayer

@export var win_jingle : AudioStream

##
## Game Over behavior script
##
## Contains all the game over screen behavior
##

## Control node that contains all the UI Nodes
@export var menu : Control

## Hide the scene and connect global signals on start
func _ready():
	set_visibility(false)
	GlobalEventHandler.game_won.connect(_on_game_won)

## Show or hide the game over screen
func set_visibility(vis: bool):
	visible = vis
	
	# We change the mouse filter to ignore or detect mouse inputs
	if(visible):
		menu.mouse_filter = Control.MOUSE_FILTER_STOP
	else :
		menu.mouse_filter = Control.MOUSE_FILTER_IGNORE


## When game over, show the game_over screen
func _on_game_won():
	set_visibility(true)
	AudioHandler.remove_all()
	AudioHandler.add_audio(win_jingle)


## On Quit button pressed, go to main menu (for html build)
func _on_quit_pressed():
	set_visibility(false)
	GlobalInputHandler.in_game = false
	get_tree().set_pause(false)
	get_tree().change_scene_to_file("res://scenes/Menu/menu_main.tscn")


## On Retry button pressed, relaunch the current scene
func _on_retry_pressed():
	set_visibility(false)
	get_tree().set_pause(false)
	get_tree().reload_current_scene()
