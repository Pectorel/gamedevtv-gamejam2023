extends Node

var in_game = false

# On ready we put the autoload to be never paused
func _ready():
	process_mode = Node.PROCESS_MODE_ALWAYS

func _unhandled_input(event):
	
	if(event.is_action_pressed("pause_menu")):
		if(in_game):
			var paused = get_tree().paused
			GlobalEventHandler.pause.emit(!paused)
			get_tree().set_pause(!paused)
